var express = require('express');
var router = express.Router();

const db = require('../db')

/* GET users listing. */
router.get('/', async (req, res, next) => {
  const result = await db.query('SELECT id, name FROM public.categories;')
  res.json(result.rows);
});

router.post('/', async (req, res, next) => {
    const { name } = req.body;
    const result = await db.query('INSERT INTO public.categories (name) VALUES ($1) RETURNING *', [name]);
    res.json(result.rows.length); 
});


module.exports = router;
