var express = require('express');
var router = express.Router();

const db = require('../db')

router.get('/', async (req, res, next) => {
  const result = await db.query('SELECT n.name, n.id, n.date, n.content, n.view_count, ca.name as category FROM public.news n, public.categories ca where n.category = ca.id;');
  res.json(result.rows);
});

router.post('/', async (req, res, next) => {
  const result = await db.query('INSERT INTO public.news(name, category, date, content, view_count) VALUES ($1, $2, CURRENT_TIMESTAMP, $3, 0) RETURNING *',
                                [req.body.name, req.body.category, req.body.content]);
  res.json(result.rows.length);
});

router.post('/add-view', async (req, res, next) => {
  const { id } = req.body;
  const result = await db.query('UPDATE public.news SET view_count = view_count + 1 where id = $1', [id]);
  res.json(result.rows.length);
});

module.exports = router;
