import React, { useState } from "react";
import {
  Menu,
  IconButton,
  makeStyles,
  MenuItem,
  Dialog,
} from "@material-ui/core";
import { Add } from "@material-ui/icons";
import AddCategory from "./AddCategory";
import AddStory from "./AddStory";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function AddMenu() {
  const classes = useStyles();
  const [dialog, setDialog] = useState("CLOSED");
  const [anchorEl, setAnchorEl] = useState(null);

  return (
    <>
      <IconButton onClick={(e) => setAnchorEl(e.currentTarget)}>
        <Add />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
      >
        <MenuItem
          onClick={() => {
            setAnchorEl(null);
            setDialog("STORY");
          }}
        >
          Add Story
        </MenuItem>
        <MenuItem
          onClick={() => {
            setAnchorEl(null);
            setDialog("CATEGORY");
          }}
        >
          Add Category
        </MenuItem>
      </Menu>
      <Dialog open={dialog !== "CLOSED"}>
        {dialog === "CATEGORY" ? (
          <AddCategory
            onCancel={() => setDialog("CLOSED")}
            onCreate={(category) => {
              setDialog("CLOSED");
              axios.post("/categories", {
                name: category,
              });
            }}
          />
        ) : (
          <AddStory
            onCancel={() => setDialog("CLOSED")}
            onCreate={(story) => {
              setDialog("CLOSED");
              axios.post("/news", {
                ...story,
              });
            }}
          />
        )}
      </Dialog>
    </>
  );
}
