import React, { useState, useEffect } from "react";
import {
  Menu,
  IconButton,
  makeStyles,
  MenuItem,
  Dialog,
  DialogTitle,
  TextField,
  DialogContent,
  DialogActions,
  Button,
} from "@material-ui/core";
import CategorySelect from "../MainPage/CategorySelect";
import axios from "axios";

export default function AddStory({ onCancel, onCreate }) {
  const [name, setName] = useState("");
  const [category, setCategory] = useState("");
  const [categories, setCategories] = useState([]);
  const [content, setContent] = useState("");

  useEffect(() => {
    axios.get("/categories").then((res) => {
      setCategories(res.data);
    });
  }, []);

  return (
    <>
      <DialogTitle>Add Story</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          label="Name"
          fullWidth
          onChange={(e) => setName(e.target.value)}
          value={name}
        />
        <CategorySelect
          categories={categories}
          category={category}
          onChange={(e) => setCategory(e.target.value)}
        />
        <TextField
          autoFocus
          margin="dense"
          label="Content"
          fullWidth
          multiline
          onChange={(e) => setContent(e.target.value)}
          value={content}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} color="primary">
          Cancel
        </Button>
        <Button
          onClick={() =>
            onCreate({ name, category: category.id, content, date: new Date() })
          }
          color="primary"
        >
          Create
        </Button>
      </DialogActions>
    </>
  );
}
