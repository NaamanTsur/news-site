import React, { useState } from "react";
import {
  DialogTitle,
  TextField,
  DialogContent,
  DialogActions,
  Button,
} from "@material-ui/core";

export default function AddCategory({ onCancel, onCreate }) {
  const [input, setInput] = useState("");
  return (
    <>
      <DialogTitle>Add Category</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          label="Category"
          fullWidth
          onChange={(e) => setInput(e.target.value)}
          value={input}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} color="primary">
          Cancel
        </Button>
        <Button onClick={() => onCreate(input)} color="primary">
          Create
        </Button>
      </DialogActions>
    </>
  );
}
