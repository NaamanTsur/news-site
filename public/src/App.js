import React, { useState } from "react";
import {
  CssBaseline,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Menu, Home, Add } from "@material-ui/icons";
import MainPage from "./MainPage/MainPage";
import AddMenu from "./Add/AddMenu";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function App() {
  const classes = useStyles();
  const [story, setStory] = useState("NONE");
  const [isAdmin, setIsAdmin] = useState(false);

  const updateViewCount = (newStory) => {
    axios.post("/news/add-view", {
      id: newStory.id,
    });
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            onClick={() => setStory("NONE")}
            className={classes.menuButton}
            color="inherit"
            aria-label="Home Page"
          >
            <Home />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {`News - ${isAdmin ? "Admin" : "Regular"} User`}
          </Typography>
          {isAdmin ? <AddMenu /> : null}
          <Button onClick={() => setIsAdmin(!isAdmin)} color="inherit">
            {isAdmin ? "Logout" : "Login"}
          </Button>
        </Toolbar>
      </AppBar>
      {story === "NONE" ? (
        <MainPage
          onStoryClick={(newStory) => {
            setStory(newStory);
            updateViewCount(newStory);
          }}
        />
      ) : (
        story.content
      )}
    </div>
  );
}
