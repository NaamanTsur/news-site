import React, { useState, useEffect } from "react";
import { FormControl, InputLabel, Select, MenuItem } from "@material-ui/core";
import axios from "axios";

const CategorySelect = ({
  className = "",
  onChange,
  category,
  categories = [],
}) => {
  return (
    <FormControl className={className} fullWidth variant="outlined">
      <InputLabel id="category">Category</InputLabel>
      <Select
        labelId="category"
        value={category}
        label="Category"
        onChange={onChange}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {categories.map((currentCategory) => (
          <MenuItem key={currentCategory.id} value={currentCategory}>
            {currentCategory.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default CategorySelect;
