import React, { useState, useEffect } from "react";
import {
  List,
  ListItem,
  ListItemText,
  Paper,
  makeStyles,
  Grid,
} from "@material-ui/core";
import { DatePicker, TimePicker } from "@material-ui/pickers";
import CategorySelect from "./CategorySelect";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  card: {
    paddingTop: theme.spacing(4),
  },
  category: {
    width: 150,
  },
}));

const createDateString = (date) => {
  const jsonDate = date.toJSON();
  const dateString =
    jsonDate.slice(8, 10) +
    "/" +
    jsonDate.slice(5, 7) +
    "/" +
    jsonDate.slice(0, 4) +
    " " +
    jsonDate.slice(11, 16);

  return dateString;
};

const MainPage = ({ onStoryClick }) => {
  const classes = useStyles();
  const [news, setNews] = useState([]);
  const [category, setCategory] = useState("");
  const [categories, setCategories] = useState([]);
  const [timeDate, setTimeDate] = useState(null);
  const [date, setDate] = useState(null);

  useEffect(() => {
    axios.get("/news").then((res) => {
      setNews(res.data);
    });

    axios.get("/categories").then((res) => {
      setCategories(res.data);
    });
  }, []);

  const filterByDateTime = (storyDate) => {
    const doesDateMatch =
      date != null
        ? date.month() === storyDate.getMonth() &&
          date.date() === storyDate.getDate()
        : true;
    const doesTimeMatch =
      timeDate != null
        ? timeDate.hour() === storyDate.getHours() &&
          timeDate.minute() === storyDate.getMinutes()
        : true;
    return doesDateMatch && doesTimeMatch;
  };

  return (
    <Paper className={classes.card}>
      <Grid container justify="center" spacing={3}>
        <Grid item>
          <CategorySelect
            categories={categories}
            className={classes.category}
            category={category}
            onChange={(e) => setCategory(e.target.value)}
          />
        </Grid>
        <Grid item>
          <DatePicker
            value={date}
            onChange={setDate}
            clearable
            inputVariant="outlined"
            label="Date"
          />
        </Grid>
        <Grid item>
          <TimePicker
            clearable
            ampm={false}
            inputVariant="outlined"
            label="Time"
            value={timeDate}
            onChange={setTimeDate}
          />
        </Grid>
      </Grid>

      <List component="nav">
        {news
          .filter((story) =>
            story.category.includes(category !== "" ? category.name : category)
          )
          .filter((story) => filterByDateTime(new Date(story.date)))
          .map((story) => (
            <ListItem key={story.id} button onClick={() => onStoryClick(story)}>
              <ListItemText
                primary={story.name}
                secondary={`${story.category} - ${createDateString(
                  new Date(story.date)
                )} - ${story.view_count} views`}
              />
            </ListItem>
          ))}
      </List>
    </Paper>
  );
};

export default MainPage;
